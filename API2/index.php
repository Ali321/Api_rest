<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:32
 */

require_once 'vendor/autoload.php';

use Pecee\SimpleRouter\SimpleRouter;
require_once 'routes.php';

// Start the routing
SimpleRouter::start();


?>