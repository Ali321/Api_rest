<?php

require_once 'controllers/Controller.php';
require_once 'models/Subscriber.php';

class SubscriberController extends Controller {
    public function getAll() {
        $subscriber = new Subscriber();
        return json_encode($subscriber->getAll());
    }

    public function getWithId($id) {
        echo json_encode('Mon id en param : ' . $id);
    }
}

// require_once 'controllers/Controller.php';
// require_once './models/Meetup.php';

// class MeetupController extends Controller {
//     // récupere la liste des meetups
//     function all() {
//         $meetup = new MeetupModel;
//         $data = $meetup->all();
//         return json_encode($data);
//     }
// }
?>