<?php

use Pecee\SimpleRouter\SimpleRouter;
require_once 'controllers/Controller.php';
use \Firebase\JWT\JWT;

class LoginController extends Controller {
    public function login() {
        if (isset($_POST['login']) && isset($_POST['passwd']) && $_POST['login'] === 'simplon' && $_POST['passwd'] === '2018') {
            $key = "JeChoisiUneCleCompliquee";
            $token = array(
                "first_name" => "Guillaume",
                "last_name" => "Chanteloube",
                "date" => 1356999524,
                "privateKey" => "simplonlyon"
            );

            $jwt = JWT::encode($token, $key);
            echo json_encode(['login' => 'success', 'token' => $jwt]);
        } else {
            // Pas de paramètres envoyés
            echo json_encode(['login' => 'error']);
        }
    }

    public function error() {
        echo json_encode(['login' => 'error']);
    }
}