<?php

require_once 'controllers/Controller.php';
require_once './models/Meetup.php';
require_once './DB/config.php';
class MeetupController extends Controller {
    // récupere la liste des meetups
    public function all() {
        $meetup = new MeetupModel;
        $data = $meetup->all();
        return json_encode($data);
    }


    public function addMeetup(){

        $titre = $_POST['titre'];
        $description = $_POST['description'];
        $date = $_POST['date'];
        $location = $_POST['location'];

       $newMeetup= MeetupModel::addMeetup($titre, $description, $location, $date);

        return json_encode($newMeetup);
    }

    public function delete($id){
        MeetupModel::deleteMeetup($id);
    }

     public function update($id){
         $data = $this->getHTTPData();
         echo "toto";
         var_dump($data);
         $titre = $data['titre'];
         $location = $data['location'];
         $date = $data['date'];
         $description = $data['description'];
         MeetupModel::UpdateMeetup($titre,$location,$date,$description,$id);
     }

    public function formUpdate($id){
        return json_encode(MeetupModel::getFormUpdate($id));
    }
}
?>