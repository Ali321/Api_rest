<?php

class Controller {
    public function getHTTPData(){
        if($_SERVER['REQUEST_METHOD'] == 'GET') return $_GET;

        if($_SERVER['REQUEST_METHOD'] == 'POST') return $_POST;

        parse_str(file_get_contents("php://input"), $data);
        return $data;
    }
}