<?php
    class Db {
        public static function connect(){
            include_once ('config.php');
            try{
            $bdd = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION) );
            return $bdd;
        }

            catch (Exception $e){
            die ('Erreur :'.$e->getMessage());
        }
    }
}
?>
