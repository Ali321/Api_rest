<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:38
 */

use Pecee\SimpleRouter\SimpleRouter;
require_once 'APIMiddleware.php';
require_once 'controllers/LoginController.php';
require_once 'controllers/DefaultController.php';
require_once 'controllers/SubscriberController.php';
require_once 'controllers/MeetupController.php';

// on ajoute un préfixe car le site se trouve dans mon cas à l'adresse :
// http://localhost/simplon/routeur/
$prefix = '/exoguillaume/Api_rest/API2';

SimpleRouter::group(['prefix' => $prefix], function () {
    SimpleRouter::post('/meetups','MeetupController@addMeetup');
    SimpleRouter::get('/meetups/{id}', 'MeetupController@formUpdate');
    SimpleRouter::get('/subscribers', 'SubscriberController@getAll');
    SimpleRouter::post('/subscribers/{id}', 'SubscriberController@getWithId');
    SimpleRouter::get('/meetups', 'MeetupController@all');
    SimpleRouter::delete('/meetups/{id}', 'MeetupController@delete');
    SimpleRouter::patch('/meetups/{id}', 'MeetupController@update');
});
?>