<?php

/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 26/02/18
 * Time: 21:48
 */

use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;
use Pecee\SimpleRouter\SimpleRouter;
use \Firebase\JWT\JWT;

class APIMiddleware implements IMiddleware {

    public function handle(Request $request) {
        // Check si le token est valide
        if (!empty($_POST['token'])) {
            $key = "JeChoisiUneCleCompliquee";
            try {
                $decoded = JWT::decode($_POST['token'], $key, array('HS256'));
                if ($decoded->privateKey !== 'simplonlyon') {
                    $this->redirectError($request);
                }
            } catch (Exception $e) {
                $this->redirectError($request);
            }
        } else {
            $this->redirectError($request);
        }
    }

    private function redirectError ($request) {
        $url = SimpleRouter::getUrl('login');
        $request->setRewriteUrl($url);
        return $request;
    }
}