<?php
require_once './DB/config.php';
// //Recupère dans la base de données
class MeetupModel{
        public function all(){
        $recup= connect();
        //query quand tu selectionne
        $_request= $recup->query("SELECT * FROM meetup");
        //PDO/query/fetchAll/SQL
        return $_request->fetchAll(PDO::FETCH_ASSOC);
        }

    public static function addMeetup($titre, $description, $location, $date) {
        $bdd = connect();
        $request = $bdd->prepare('INSERT INTO meetup (titre, location, description, date)
        VALUES (:a, :b, :c, :d);');
        $request->execute([
            'a' => $titre,
            'b' => $location,
            'c' => $description,
            'd' => $date,
        ]);
        return [
            'id' => $bdd->lastInsertId(),
            'title' => $titre,
            'location' => $location,
            'description' => $description,
            'date' => $date,
            ];
    }

    public static function deleteMeetup($id){
        $bdd = connect();
        $request = $bdd->prepare('DELETE FROM meetup WHERE id=:id');
        $request->execute([
            'id'=>$id
        ]);
    }

    public static function getFormUpdate($id){
        $bdd = connect();
        $request = $bdd->prepare('SELECT * FROM meetup WHERE id=:id');
        $request->execute([
            'id'=>$id
        ]);
        return $request->fetchAll(PDO::FETCH_ASSOC);
    }
    public static function UpdateMeetup($titre,$location,$date,$description,$id){
        $bdd = connect();
        $request = $bdd->prepare('UPDATE `meetup` SET  `titre`=:titre,`date`=:date,`location`=:location,`description`=:description WHERE id=:id');
        $request->execute([
            'id'=>$id,
            'titre'=>$titre,
            'location'=>$location,
            'date'=>$date,
            'description'=>$description
        ]);
    }
}
?>